﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/notificationhub").build();


connection.on("ReceiveMessage", function (user, message) {

    console.log('Message Received: ', user, message);

    var msg = message.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
    var encodedMsg = user + " says " + msg;
    var li = document.createElement("li");
    li.textContent = encodedMsg;
    document.getElementById("resTime").innerHTML = "Message Received: " + user + message;
});

connection.start().then(function () {
    console.log('Signalr Established');
}).catch(function (err) {
    return console.error(err.toString());
});

