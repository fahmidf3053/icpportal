﻿function getBranches() {
    var selectBank = $("#Input_FinInfoBankId option:selected").text();
    console.log(selectBank)
    if (selectBank != "Select Financial Institution") {
        var paymentTypeName = $("#Input_FinInfoBankId option:selected").val();
        //console.log(paymentTypeName);

        $.ajax({
            url: '/Users/getBranches/?id=' + paymentTypeName,
            type: 'get',
            contentType: 'application/json',
            success: function (data) {
                $("#Input_FinInfoBranchId").empty();

                var length = Object.keys(data).length;
                for (i = 0; i < length; i++) {
                    var o = new Option(data[i].branchName, data[i].id);
                    //console.log(data[i].branchName)
                    $("#Input_FinInfoBranchId").append(o);
                }
            },
        });
    }
    else {
        $("#Input_FinInfoBranchId").empty();
        var o = new Option("Branch Name");
        $("#Input_FinInfoBranchId").append(o);
    }
}
$(document).ready(function () {
    
    $('#finInfoTable').dataTable({
        "searching": false,   // Search Box will Be Disabled
        "ordering": false,    // Ordering (Sorting on Each Column)will Be Disabled
        "info": false,         // Will show "1 to n of n entries" Text at bottom
        "lengthChange": false, // Will Disabled Record number per page
        "paging": false,
        
    })
    $('#AddNew').click(function () {
        $('#FI_Add_Form').css('display', 'block')
        $('#AddNew').css('display', 'none')
    });
    $('#SaveNewFI').click(function () {
        var valid = true;
        var bankId = $('#Input_FinInfoBankId').val();
        var branchId = $('#Input_FinInfoBranchId').val();
        var accountNumber = $('#Input_FinInfoAccountNumber').val();
        if (branchId == "" || branchId == null) {
            branchId = '0';
        }
        if (accountNumber == "" || accountNumber == null) {
            valid = false;
            $('#AccNumberWarning').html('Account number cannot be empty');
        }
        if (bankId == "0" || bankId == null) {
            valid = false;
            $('#AccNumberWarning').html('Please select a financial institution');
        }
        var userId = $('#USER_ID').val();
        console.log(userId)
        var data = {};
        data["financialInstitutionId"] = bankId;
        data["branchId"] = branchId;
        data["accountNumber"] = accountNumber;
        data["userId"] = userId;
        //console.log(JSON.stringify(data));
        if (valid) {
            $('#AccNumberWarning').html('');
            
            $.ajax({
                url: '/Profile/AddNewFI',
                data: JSON.stringify(data),
                type: "POST",
                dataType: 'JSON',
                contentType: "application/json",
                success: function (data) {
                    
                    console.log(data)
                    if (data['error'] != "" || data['error'] == null) {
                        $('#AccNumberWarning').html(data['error']);
                    }
                    else {
                        
                        $(this).prop("disabled", false);
                        //console.log(data);
                        $("#Input_FinInfoBankId").val(0);
                        $("#Input_FinInfoBranchId").empty();
                        var o = new Option("Branch Name");
                        $("#Input_FinInfoBranchId").append(o);
                        $("#Input_FinInfoAccountNumber").val("")
                        var atag = `<a href="/profile/EnableOrDisableFinancialInformation?FinInfoId=` + data['fiId'] + `" style="color:#0000EE;"> Disable </a>` +
                            `<a href="/profile/CloseAccount?FinInfoId=` + data['fiId'] + `" style="color:#0000EE;padding-left:10px;"> Close </a>`

                        //$('#finInfoTable > tbody').append(`<tr><td>` + data['fiName'] + `</td><td>` + data['accountNumber'] + `</td><td>` + data['fiBranch'] + `</td></tr>`);
                        $('#finInfoTable').DataTable().row.add([
                            `<input type = "radio" id = "" name = "DefaultFI" value = "` + data['id'] + `"/>`,
                            data['fiName'],
                            data['accountNumber'],
                            data['fiBranch'],
                            atag
                        ]).draw(false);
                        swal("Added Successfully!", "Financial Instiution Information Added", "success");
                    }
                },
                error: function (data) {
                    console.log(data);
                    swal("Error!", data.responseJSON['errorMessage'], "error");
                }
            });
        }
        
    });
})