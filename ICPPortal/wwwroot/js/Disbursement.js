﻿function ShowPinPopup() {
    $('#idtpPinManual').val("");
    $('#pinConfirmationModal').modal('show');
}

function CancelPin() {
    $('#idtpPinManual').val("");
};


function AddRow() {

    if ($("#receiverTable tbody tr[class=odd]").length) {
        $("#receiverTable tbody tr[class=odd]").remove();
    }

    var tempAmount = $("#amount").val();
    var vid = $("#receiverVID").val().trim();

    if (!tempAmount || !vid || isNaN(tempAmount)) {
        swal('Invalid Data', 'Enter Reciever \nInfo!', 'error');
        return;
    }

    balance = parseFloat(balance.toString().replace(/,/g, ''));
    var amount = parseFloat(tempAmount)
    if (balance < amount) {
        $("#amount").focus();
        document.getElementById('AmountWarning').innerHTML = "You don't have sufficient balance.";
        return false;
    }
    let duplicate = false;
    $('#receiverTable tbody tr').each(function (i, row) {
        let reId = $(this).find('[name ="vid"]').text().trim();

        if (vid === reId) {
            swal('Duplicate Data', 'Reciever already exists!', "error");
            duplicate = true;
            return false;
        }
    });
    if (duplicate) return;
    document.getElementById('AmountWarning').innerHTML = "";
    var markup = "<tr><td name='vid'>" + vid + "</td><td name='amount' id='tdAmount'>" + tempAmount + "</td><td style='width: 70px;'><button class='btn btn-danger' onclick='RemoveRow()'>Remove</button></td></tr>";
    $("#receiverTable tbody").append(markup);

    var sum = 0;
    $('#receiverTable tbody tr').each(function (i, row) {
        var $row = $(row);
        var q = $(this).find('#tdAmount').text().trim();
        sum += parseFloat(q);
    });
    $('#valTotal').text(sum);
}

function RemoveRow() {
    var td = event.target.parentNode;
    var tr = td.parentNode;
    tr.parentNode.removeChild(tr);

    var sum = 0;
    $('#receiverTable tbody tr').each(function (i, row) {
        var $row = $(row);
        var q = $(this).find('#tdAmount').text().trim();
        sum += parseFloat(q);
    });
    $('#valTotal').text(sum);
}

function InvokeCreateDisbursement(ip, latlong, location) {

    var data = {};
    var totalAmount = $('#valTotal').text().trim();
    var distype = $("#disbursmentType").val();
    var ChannelType = 1; //$("#channel").val();
    var Purpose = $("#purpose").val();
    var pin = $("#idtpPinManual").val();
    data["Amount"] = totalAmount;
    data["DisbursmentType"] = parseInt(distype);
    data["ChannelType"] = parseInt(ChannelType);
    data["IP"] = ip;
    data["LatLong"] = latlong;
    data["Location"] = location;
    data["Purpose"] = Purpose;
    data["IDTPPIN"] = pin;
    data.ReceiVerList = [];

    $('#receiverTable tbody tr').each(function (i, row) {

        var $row = $(row);
        var receiver = {};

        var reId = $(this).find('[name ="vid"]').text().trim();
        var reAmount = $(this).find('[name ="amount"]').text().trim();

        if (reAmount) {
            receiver['ReceiverVId'] = reId;
            receiver['Amount'] = reAmount;
            data.ReceiVerList.push(receiver);
        }
    });
    console.log(JSON.stringify(data));

    if (!(data.ReceiVerList && data.ReceiVerList.length > 0)) {

        swal('Invalid Data', 'Enter Receiver Info', "error");
        return;
    }

    $.ajax({
        url: '/DisbursmentUI/CreateDisbursment',
        type: "POST",
        dataType: 'JSON',
        contentType: "application/json",
        data: JSON.stringify(data),
        success: function (data) {
            $('#preloader').hide();
            swal("Successfull!", data['messase'], "success");
            $('.available-limit').val(data.limit);
            $('.user-balance').val(data.balance);
            $('.sending-limit').val(data.sendingLimit);
            ClearScreen();
        },
        error: function (data) {
            $('#preloader').hide();
            var errorMsg = data.responseJSON['errorMessage'];
            var errorTran = data.responseJSON['errorTran'];
            ClearScreen();
            ShowErrorTran(errorTran, errorMsg);
        }
    });
}

function ClearScreen() {

    $('#receiverVID').val('');
    $('#amount').val('');
    $('#purpose').val('');
    $('#IDTPPIN').val('');
    $('#FullName').val('');
}

function ShowErrorTran(errorTran, errorMsg) {

    if (errorTran) {
        var arr = JSON.parse(errorTran.replace(/&quot;/g, '"'));
        errorMsg = '';
        arr.forEach(function (arrayItem) {
            var msg = arrayItem.ReceiverVId + '  ----  ' + arrayItem.FailureReason + '\n';
            errorMsg += msg;
        });
    }

    if (errorMsg) swal("Error!", errorMsg, "error");
}