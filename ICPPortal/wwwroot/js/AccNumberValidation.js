﻿$("#Input_FinInfoAccountNumber").keypress(function (event) {
    $('#AccNumberWarning').html('');
    var character = String.fromCharCode(event.keyCode);
    return isValid(character);
});
function isValid(str) {
    return !/[~`!@@#$%\^&*()+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
}