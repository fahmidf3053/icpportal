﻿namespace ICPPortal.Models.DTOs
{
    public class DeviceInfoDto
    {
        public string DeviceId { get; set; }
        public string VirtualId { get; set; }
    }
}
