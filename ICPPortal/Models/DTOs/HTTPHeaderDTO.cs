﻿namespace ICPPortal.Models.DTOs
{
    public class HTTPHeaderDTO
    {
        public string FIVID { get; set; }
        public string FIDeviceID { get; set; }
    }
}
