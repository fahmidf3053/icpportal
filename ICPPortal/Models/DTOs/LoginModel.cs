﻿namespace ICPPortal.Models.DTOs
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        
    }
}
