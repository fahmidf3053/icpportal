﻿namespace ICPPortal.Models.DTOs
{
    public class RegisterIPAddressDTO
    {
        public string StartingIpAddress { get; set; }
        public string EndingIpAddress { get; set; }
        public string CreatedOn { get; set; }

    }
}
