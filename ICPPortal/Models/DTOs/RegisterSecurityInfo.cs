﻿namespace ICPPortal.Models.DTOs
{
    public class RegisterSecurityInfo
    {
        public string FIPrivateKey { get; set; }
        public string IDTPPublicKey { get; set; }
        public string CreatedOn { get; set; }
    }
}
