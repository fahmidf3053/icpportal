﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Xml;
using ICPPortal.Helper;
using ICPPortal.Models;
using ICPPortal.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace ICPPortal.Controllers
{
    public class HomeController : Controller
    {
        private DBHelper dBHelper = new DBHelper();
        //private readonly ILogger<HomeController> _logger;

        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}
        
        public IActionResult Index()
        {
            try
            {
                string res = HttpContext.Session.GetString("RESP");
                if (res == "Success")
                {
                    string tableName = Constants.tableName;
                    DBHelper dBHelper = new DBHelper();
                    string idtpUrl = dBHelper.ReadData(tableName, Constants.KeyIDTPPortalURL);
                    string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                    string bankVID = dBHelper.ReadData(tableName, Constants.keyFIVID);

                    ViewBag.IDTPUrl = idtpUrl;
                    ViewBag.bankName = bankName;
                    TempData["BankVID"] = bankVID;
                    return View();
                }
                else
                {
                    return RedirectToAction(nameof(LogIn));
                }
            }
            catch (Exception ex) {
                throw ex;
            }
           
        }

        [HttpGet]
        public IActionResult LogOut()
        {
            try {
                HttpContext.Session.SetString("RESP", "");
                TempData["LogOut"] = "True";
                return RedirectToAction(nameof(LogIn));
            }
            catch (Exception ex) {
                throw ex;
            }
            
        }

        [HttpGet]
        public IActionResult LogIn()
        {
            try {
                string tableName = Constants.tableName;
                string bankVID = dBHelper.ReadData(tableName, Constants.keyFIVID);
                TempData["VID"] = bankVID;
                string idtpUrl = dBHelper.ReadData(tableName, Constants.KeyIDTPURL);
                ViewBag.IDTPUrl = idtpUrl;
                ViewBag.check = "true";
                return View();
            }
            catch (Exception ex) {
                throw ex;
            }

          
        }

        [HttpPost]
        public IActionResult LogIn([FromForm] LoginModel loginModel)
        {
            try
            {
                //HttpContext.Session.SetString("RESP", "Success");
                //return RedirectToAction(nameof(Index));
                string tableName = Constants.tableName;
                DBHelper dBHelper = new DBHelper();
                string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                ViewBag.bankName = bankName;
                string VID = dBHelper.ReadData(tableName, Constants.keyFIVID);
                string deviceIdDB = dBHelper.ReadData(tableName, Constants.keyDeviceID);
                string deviceIdReal = GetMacAddress().ToString();
                if (VID == loginModel.UserName)
                {
                    string regXML = string.Empty;
                    string BICCode = dBHelper.ReadData(tableName, Constants.keyBICCode);
                    // Machine generated device fingerprint check
                    if (deviceIdDB == deviceIdReal)
                    {
                        regXML = CustomXMLGenerator.GetFIUserPassValidationXML(VID, BICCode, loginModel.Password, deviceIdReal);
                    }
                    // Initial device fingerprint check at first launch of ICP Portal
                    else if (deviceIdDB == "000000000000")
                    {
                        regXML = CustomXMLGenerator.GetFIUserPassValidationXML(VID, BICCode, loginModel.Password, deviceIdDB);
                    }
                    else
                    {
                        TempData["message"] = "Please register your device";
                        return RedirectToAction(nameof(LogIn));
                    }
                    var baseUrl = HttpClientHelper.PrepareURL(Constants.validateFIPasswordEndPoint);
                    var uri = new Uri(baseUrl);

                    var headerDTO = DBHelper.GetHttpHeaders();
                    var responseResult = HttpClientHelper.Post(uri, regXML, headerDTO);
                    responseResult = responseResult.Replace("\\", "", StringComparison.CurrentCulture);
                    responseResult = responseResult[1..^1];
                    responseResult = responseResult.Replace(@"\", "", StringComparison.CurrentCulture);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(responseResult);
                    string message = doc.GetElementsByTagName("Message").Item(0).InnerText;
                    if (message == "Success")
                    {
                        HttpContext.Session.SetString("RESP", message);
                        return RedirectToAction(nameof(Index));
                    }
                    TempData["message"] = message;

                    return RedirectToAction(nameof(LogIn));
                }
                ViewBag.message = "Invalid User Name";
                return RedirectToAction(nameof(LogIn));
            }

            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction(nameof(LogIn));
            }
        }

        private static PhysicalAddress GetMacAddress()
        {
            return (from nic in NetworkInterface.GetAllNetworkInterfaces() where nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet && nic.OperationalStatus == OperationalStatus.Up select nic.GetPhysicalAddress()).FirstOrDefault();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
