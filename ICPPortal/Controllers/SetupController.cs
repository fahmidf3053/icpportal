﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Xml;
using ICPPortal.Helper;
using ICPPortal.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ICPPortal.Controllers
{
    public class SetupController : Controller
    {
        private DBHelper dBHelper = new DBHelper();

        public IActionResult Index()
        {
            string res = HttpContext.Session.GetString("RESP");
            if (res == "Success")
            {
                string tableName = Constants.tableName;

                string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                ViewBag.bankName = bankName;

                return View();

            }
            else {
                return RedirectToAction("LogIn", "Home");
            }
                
        }

        private static PhysicalAddress GetMacAddress()
        {
            return (from nic in NetworkInterface.GetAllNetworkInterfaces() where nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet && nic.OperationalStatus == OperationalStatus.Up select nic.GetPhysicalAddress()).FirstOrDefault();
        }

    }
}

