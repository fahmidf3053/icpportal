﻿using ICPPortal.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ICPPortal.Controllers
{
    public class SetupEndpointsController : Controller
    {
        public IActionResult Index(string check = null)
        {
            try
            {
                string res = HttpContext.Session.GetString("RESP");
                if (res == "Success")
                {
                    string tableName = Constants.tableName;
                    DBHelper dBHelper = new DBHelper();
                    string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                    ViewBag.bankName = bankName;
                    string PIMURL = dBHelper.ReadData(tableName, Constants.KeyPIMURL);
                    string IDTPAPIURL = dBHelper.ReadData(tableName, Constants.KeyIDTPURL);
                    string IDTPPortalURL = dBHelper.ReadData(tableName, Constants.KeyIDTPPortalURL);
                    ViewBag.PIMURL = PIMURL;
                    ViewBag.IDTPAPIURL = IDTPAPIURL;
                    ViewBag.IDTPPortalURL = IDTPPortalURL;

                    if (check == null)
                    {
                        return View();
                    }
                    return RedirectToAction("Index", "Setup");
                }

                else
                {
                    return RedirectToAction("LogIn", "Home");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpPost]
        public IActionResult Index(string PIMPortal, string IDTPURL, string IDTPAPI)
        {
            try
            {
                string res = HttpContext.Session.GetString("RESP");
                if (res == "Success")
                {
                    string tableName = Constants.tableName;
                    DBHelper dBHelper = new DBHelper();
                    string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                    ViewBag.bankName = bankName;

                    bool flag = dBHelper.InsertData(tableName, PIMPortal, Constants.KeyPIMURL);
                    bool flag1 = dBHelper.InsertData(tableName, IDTPAPI, Constants.KeyIDTPURL) ;
                    bool flag2 = dBHelper.InsertData(tableName, IDTPURL, Constants.KeyIDTPPortalURL) ;
                    
                    
                    return Json(flag && flag1 && flag2 == true ? "Endpoints saved successfully" : "updation failed");
                }
                else
                {
                    return Json("Session Expired");  
                }
            }
            catch (Exception ex)
            {
                return Json("updation failed for" + ex.Message);
            }
        }
    }
}
