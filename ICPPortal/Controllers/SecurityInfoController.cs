﻿using ICPPortal.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ICPPortal.Controllers
{
    public class SecurityInfoController : Controller
    {
        public IActionResult Index(string check = null)
        {
            try {
                string res = HttpContext.Session.GetString("RESP");
                if (res == "Success")
                {
                    string tableName = Constants.tableName;
                    DBHelper dBHelper = new DBHelper();
                    string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                    ViewBag.bankName = bankName;
                    string securityKey = dBHelper.ReadData(tableName, Constants.keyFIPrivateKey);
                    string publicKey = dBHelper.ReadData(tableName, Constants.keyFIPublicKey);
                    ViewBag.SecurityKey = securityKey;
                    ViewBag.IDTPPublicKey = publicKey;
                    if (check == null)
                    {
                        return View();
                    }
                    return RedirectToAction("Index", "Setup");
                }

                else
                {
                    return RedirectToAction("LogIn", "Home");
                }
            }
            catch (Exception ex) {
                throw ex;
            }
           
        }

        
        
    }
}
