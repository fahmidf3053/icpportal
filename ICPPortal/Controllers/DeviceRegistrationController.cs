﻿using System;
using System.Linq;
using System.Net.NetworkInformation;
using System.Xml;
using ICPPortal.Helper;
using ICPPortal.Models.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ICPPortal.Controllers
{
    public class DeviceRegistrationController : Controller
    {
        public IActionResult Index()
        {
            try {
                string res = HttpContext.Session.GetString("RESP");
                if (res == "Success")
                {
                    string macAddress = GetMacAddress().ToString();
                    ViewBag.MACAddress = macAddress;


                    string tableName = Constants.tableName;
                    DBHelper dBHelper = new DBHelper();
                    string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);
                    ViewBag.bankName = bankName;
                    string deviceIdDB = dBHelper.ReadData(tableName, Constants.keyDeviceID);
                    ViewBag.deviceID = deviceIdDB;
                    return View();
                }
                else
                {
                    return RedirectToAction("LogIn", "Home");
                }
            }
            catch(Exception ex){

                throw ex;
            }
           

        }
        [HttpPost]
        public IActionResult Index(DeviceInfoDto deviceInfoDto)
        {
            try
            {
                string res = HttpContext.Session.GetString("RESP");
                if (res == "Success")
                {
                    string tableName = Constants.tableName;
                    DBHelper dBHelper = new DBHelper();

                    string VID = dBHelper.ReadData(tableName, Constants.keyFIVID);
                    string bankName = dBHelper.ReadData(tableName, Constants.keyFIName);

                    ViewBag.bankName = bankName;
                    deviceInfoDto.VirtualId = VID;

                    var headerDTO = DBHelper.GetHttpHeaders();
                    string regXML = CustomXMLGenerator.GetRegisteredDeviceXML(deviceInfoDto);
                    var baseUrl = HttpClientHelper.PrepareURL(Constants.registerICPMachineEndPoint);
                    var uri = new Uri(baseUrl);

                    var responseResult = HttpClientHelper.Post(uri, regXML, headerDTO);
                    bool flag = dBHelper.InsertData(tableName, deviceInfoDto.DeviceId, Constants.keyDeviceID);
                   
                    responseResult = responseResult.Replace("\\", "", StringComparison.CurrentCulture);
                    responseResult = responseResult[1..^1];
                    responseResult = responseResult.Replace(@"\", "", StringComparison.CurrentCulture);
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(responseResult);
                   
                    string code = doc.GetElementsByTagName("Code").Item(0).InnerText;
                    string message = doc.GetElementsByTagName("Message").Item(0).InnerText;
                    return Json(code == "200" ? "Device ID registered successfully" : "Registration failed");
                }
                else
                {
                    return Json("Session Expired"); 
                }
            }
            catch (Exception ex)
            {
                return Json("Registration failed for" + ex.Message);
            }
        }

        private static PhysicalAddress GetMacAddress()
        {
            return (from nic in NetworkInterface.GetAllNetworkInterfaces() where nic.NetworkInterfaceType == NetworkInterfaceType.Ethernet && nic.OperationalStatus == OperationalStatus.Up select nic.GetPhysicalAddress()).FirstOrDefault();
        }
    }
}
