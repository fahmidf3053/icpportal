﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using ICPPortal.Models.DTOs;

namespace ICPPortal.Helper
{
    public class CustomXMLGenerator
    {
        /*public static string getRegistrationXML(
            UserDTO entity,
            List<FinancialInformationDTO> financialInformationDTOs, string orgId
            )
        {
            string financialInfo = "";
            if (financialInformationDTOs != null)
            {
                financialInfo = GenerateFinancialInformationSegment(financialInformationDTOs);
            }
            string registrationString =
           @$"<RegisterUser xmlns:idtp=""http://idtp.gov.bd/xxx/schema/"">
                <Head ver=""1.0"" ts=""{DateTime.Now.ToString()}"" orgId=""{orgId}""   msgId=""1"" /> 
                    <Req id=""{Guid.NewGuid()}"" note=""RegisterUser"" ts=""{DateTime.Now.ToString()}"" type=""RegisterUser"" />
           <Entity seqNum=""1"">
            <Info>
              <EntityType>{entity.EntityType}</EntityType>
              <Name>{entity.Name}</Name>
              <AddressLine1>{entity.AddressLine1}</AddressLine1>
              <AddressLine2>{ entity.AddressLine2}</AddressLine2>
              <District>{entity.District}</District>
              <PostalCode>{entity.PostalCode}</PostalCode>
              <MobileNumber>{entity.MobileNumber}</MobileNumber>
              <Email>{entity.Email}</Email>
              <TypeOfOwnership>{entity.TypeOfOwnership}</TypeOfOwnership>
              <TypeOfBusiness>{entity.TypeOfBusiness}</TypeOfBusiness>
              <NameOfMinistry>{entity.NameOfMinistry}</NameOfMinistry>
              <NameOfDivision>{entity.NameOfDivision}</NameOfDivision>
              <TypeOfFinancialInstitution>{entity.TypeOfFinancialInstitution}</TypeOfFinancialInstitution>
              <SwiftCode>{entity.SwiftCode}</SwiftCode>
              <CBAccountNumber>{entity.CBAccountNumber}</CBAccountNumber>
              <NID>{entity.NID}</NID>
              <TIN>{entity.TIN}</TIN>
              <BIN>{entity.BIN}</BIN>
              <Password>{entity.Password}</Password>
            </Info>
            <DeviceInfo>
                 <Device_ID>{entity.deviceId}</Device_ID>
                 <Mobile_No>{entity.deviceMobileNo}</Mobile_No>
                 <Location>{entity.location}</Location>
                 <IP>{entity.IP}</IP>
            </DeviceInfo>
            {financialInfo}
            <ContactReference>
                 <ContactPersonName> {entity.ContactPersonName}</ContactPersonName>
                 <Designation>{entity.ContactPersonDesignation}</Designation>
                 <ContactNumber>{entity.ContactPersonMobileNumber}</ContactNumber>
                 <Email>{entity.ContactPersonEmail}</Email>
            </ContactReference>
            <Creds>
               <Cred type=""IDTP_PIN"" subtype="""">
                    <Data>{entity.IDTPPIN}</Data>
               </Cred>
            </Creds>
            <RequestedVirtualID value=""{entity.VirtualID}"" />
            <OtherInfo>
                <Channel>{entity.ChannelType}</Channel>
            </OtherInfo>
            </Entity>
         </RegisterUser>";
            return registrationString;
        }
*/


        /*private static string GenerateFinancialInformationSegment(List<FinancialInformationDTO> financialInformationDTOs)
        {

            StringBuilder stringBuilder = new StringBuilder();
            foreach (var item in financialInformationDTOs)
            {
                stringBuilder.Append($@"<FinancialInstitutionInfo>
                    <FinancialInstitution>{item.FIName}</FinancialInstitution>
                    <BranchName>{item.FIBranch}</BranchName>
                    <RoutingNumber>{item.RoutingNumber}</RoutingNumber>
                    <AccountNumber>{item.AccountNumber}</AccountNumber>
                     </FinancialInstitutionInfo>");
            }
            string receiverData = $@"{stringBuilder.ToString()}";
            return receiverData;
        }*/
        public static string GetRegisteredDeviceXML(DeviceInfoDto icpMachineInfoDto)
        {
            string registrationString =
              @$"<RegisterICPMachine xmlns:idtp=""http://idtp.gov.bd/xxx/schema/"">
                <Head ver=""1.0"" ts=""{DateTime.Now.ToString()}"" orgId=""IDTP""   msgId=""1"" /> 
                    <Req id=""{Guid.NewGuid()}"" note=""RegisterICPMachine"" ts=""{DateTime.Now.ToString()}"" type=""RegisterICPMachine"" />
              <Entity seqNum=""1"">
                <Info>
                  <Virtual_ID>{icpMachineInfoDto.VirtualId}</Virtual_ID>
                  <Device_ID>{icpMachineInfoDto.DeviceId}</Device_ID>
                </Info>
              </Entity>
             </RegisterICPMachine>";
            return registrationString;
        }
        public static string GetFIUserPassValidationXML(string userName, string BICCode, string plainPassword, string deviceID)
        {
            string xmlString =
          @$"<CheckICPUserCredentials xmlns:idtp=""http://idtp.gov.bd/xxx/schema/"">
              <Head ver=""1.0"" ts=""2020-05-16T14:15:43+05:30"" orgId=""{BICCode}"" msgId = ""1""/>
              <Req id=""8ENSVVR4QOS7X1UGPY7JGUV444PL9T2C3QM"" note=""Check ICP User Credentials"" ts=""2020-05-16T14:15:42+05:30"" type=""CheckICPUserCredentials""/>
                <ChannelInfo>
                  <ChannelID>{Constants.ChannelID}</ChannelID>
                </ChannelInfo>
                <ICPUserInfo>
                  <UserInfo>
                             <UserVID value = ""{userName}""/>
                  </UserInfo>
                  <OtherInfo/>
                  <Creds>
                    <Cred type = ""ICPPASSWORD"" subtype = """">
                        <Data>{plainPassword}</Data>
                    </Cred>
                  </Creds>
               </ICPUserInfo>
           </CheckICPUserCredentials>";
            return xmlString;
        }

        public static string PrepareXMLfromDTO<T>(T s)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(T));

            var xml = "";

            using var sww = new StringWriter();
            using XmlWriter writer = XmlWriter.Create(sww);
            serializer.Serialize(writer, s);
            xml = sww.ToString();

            return xml;
        }

    }
}
