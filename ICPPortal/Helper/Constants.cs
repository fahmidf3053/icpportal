﻿using System;
using System.IO;

namespace ICPPortal.Helper
{
    public static class Constants
    {
        public static readonly string tableName = "ICPPortalInfo";
        public static readonly string KeyIDTPURL = "IDTPUrl";
        public static readonly string KeyIDTPPortalURL = "IDTPPortalUrl";
        public static readonly string KeyPIMURL = "FIURL";
        public static readonly string keyFIName = "FIName";
        public static readonly string keyFIVID = "FIVID";
        public static readonly string keyDeviceID = "DeviceID";
        public static readonly string keyFIPrivateKey = "FIPrivateKey";
        public static readonly string keyFIPublicKey = "IDTPPublicKey";
        public static readonly string keyBICCode = "SwiftBIC";
        public static readonly string keyAppVersion = "Version";
        public static readonly string ChannelID = "Online";
        public static readonly string registerICPMachineEndPoint = "/RegisterICPMachine";
        public static readonly string validateFIPasswordEndPoint = "/CheckICPUserCredentials";
        public static readonly string AUTO_SIGN_IN = "/Authentication/AutoSignIn";
        
        public static string ConnectionString {
            get
            {
                String envConnStr = Environment.GetEnvironmentVariable("ICP_SQL_LITE_CONNECTION");
                string CONNECTION_STRING = Path.Combine(Environment.CurrentDirectory, "Database", "ICPDatabase.db");
                if (!String.IsNullOrEmpty(envConnStr))
                {
                    return envConnStr;
                }
                else if (File.Exists(CONNECTION_STRING))
                {
                    return CONNECTION_STRING;
                }
                else
                {
                    CONNECTION_STRING = string.IsNullOrEmpty(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)) ?
                    Path.GetFullPath("/Database/ICPDatabase.db") :
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), @"Database\ICPDatabase.db");

                    return CONNECTION_STRING;
                }
            }
        }

      
        public static class HttpHeaderTags
        {
            public const string FI_VID = "FIVID";
            public const string FI_DeviceID = "FIDeviceID";
        }
    }
}
