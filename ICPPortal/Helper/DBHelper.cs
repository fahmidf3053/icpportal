﻿using ICPPortal.Models.DTOs;
using Microsoft.Data.Sqlite;
using System;

namespace ICPPortal.Helper
{
    public class DBHelper
    {
        public bool InsertData(string tableName, string value, string key)
        {
            try {
                var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = Constants.ConnectionString };
                using var connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
                connection.Open();
                using var insertData = connection.BeginTransaction();
                var insertCmd = connection.CreateCommand();
                insertCmd.CommandText = $"UPDATE {tableName} SET Value = '{value}' WHERE Key = '{key}'";
                insertCmd.ExecuteNonQuery();
                insertData.Commit();
                return true;
            }
            catch (Exception ex) {
                return false;
            }
        }

        public string ReadData(string tableName, string key)
        {
            try {
                var connectionStringBuilder = new SqliteConnectionStringBuilder();
                connectionStringBuilder.DataSource = Constants.ConnectionString;
                var value = string.Empty;
                using var connection = new SqliteConnection(connectionStringBuilder.ConnectionString);
                connection.Open();

                var selectCmd = connection.CreateCommand();
                selectCmd.CommandText = $"SELECT value FROM {tableName} WHERE Key = '{key}'";

                using var reader = selectCmd.ExecuteReader();
                while (reader.Read())
                {
                    var message = reader.GetString(0);
                    value = message;
                }

                return value;
            }
            catch (Exception ex) {

                return String.Empty;
            }
            
        }

        public static HTTPHeaderDTO GetHttpHeaders()
        {
            DBHelper dB = new DBHelper();
            var hDto = new HTTPHeaderDTO
            {
                FIVID = dB.ReadData(Constants.tableName,
                    Constants.keyFIVID ),
                FIDeviceID = dB.ReadData(Constants.tableName,
                    Constants.keyDeviceID)
            };
            return hDto;
        }
        public static string GetAppVersion()
        {
            DBHelper dB = new DBHelper();
            var value = dB.ReadData(Constants.tableName, Constants.keyAppVersion);
            return value;
        }
    }
}
