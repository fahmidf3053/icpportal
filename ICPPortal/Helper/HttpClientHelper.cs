﻿using ICPPortal.Models.DTOs;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ICPPortal.Helper
{
    public static class HttpClientHelper
    {
        public static string Post(System.Uri uri, string stringData, HTTPHeaderDTO headerDTO)
        {
            try
            {
                using var client = new HttpClient();
               
                if (headerDTO != null)
                {
                    client.DefaultRequestHeaders.Add(Constants.HttpHeaderTags.FI_VID, headerDTO.FIVID);
                    client.DefaultRequestHeaders.Add(Constants.HttpHeaderTags.FI_DeviceID, headerDTO.FIDeviceID);
                }
                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                using HttpResponseMessage response = client.PostAsJsonAsync(uri, stringData).Result;
                response.EnsureSuccessStatusCode();
                var result = response.Content.ReadAsStringAsync().Result;
                return result;
            }
            catch (WebException wex)
            {
                throw new WebException(wex.Message);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public static string PrepareURL(string apiEndpoint)
        {
            DBHelper dbHelper = new DBHelper();
            var url = dbHelper.ReadData(Constants.tableName,
                Constants.KeyIDTPURL);

            url = url.Trim('/');
            url += apiEndpoint;

            return url;
        }
    }
}
